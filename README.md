
# Adorable Avatars for Rust #

This project is a port of adorable-avatars [https://github.com/adorableio/avatars-api-middleware]

## License

[Same as original](https://github.com/adorableio/avatars-api-middleware), MIT.
